import os

def presionarTecla():
    input('Presione ENTER para continuar...')
    os.system('cls')

def menu():
    print('1: Registrar productos')
    print('2: Mostrar listado de productos')
    print('3: Mostrar productos que tengan entre X e Y ')
    print('4: Sumar stock X a productos menores de Y')
    print('5: Eliminar todos los productos con stock 0')
    print('6: Salir')
    eleccion = int(input('Sleccione una opción: '))
    while not(eleccion >= 1) and not(eleccion <= 6):
        eleccion = int(input('Seleccione una opcion: '))
    os.system('cls')
    return eleccion

def validarDescripcion():
    descripcion =  input('Ingrese la descripcion del producto: ')
    while descripcion == '':
        descripcion = input('Ingrese el descripcion del producto: ')
    return descripcion

def validarPrecio():
    precio = float(input('Ingrese precio: '))
    while precio < 0:
        precio = float(input('Ingrese precio: '))
    return precio       

def validarStock():
    stock = int(input('Ingrese stock: '))
    while stock < 0:
        stock = int(input('Ingrese stock: '))
    return stock 

def cargarProductos():
    print('Cargar Productos: ')
    productos = {}
    codigo = -1
    while codigo != 0:
        codigo = int(input('Código (0 para finalizar carga): '))
        if codigo != 0:
            if codigo not in productos:
                descripcion = validarDescripcion()
                precio = validarPrecio()
                stock = validarStock()
                productos[codigo] = [descripcion, precio, stock]
                print('Producto Agregado...')
            else:
              print('El producto ya existe')
    return productos

def mostrar(diccionario):
    print('Cod: Descripcion/Precio/Stock')
    for codigo, datos in diccionario.items():
        print(codigo, datos)

def mostrarIntervalos(diccionario):
    print('Escriba entre que intervalos de stock desea mostrar: ')
    min = int(input('Desde: '))
    max = int(input('Hasta: '))
    print('Cod: Descripcion/Precio/Stock')
    for codigo, datos in diccionario.items():
        if (datos [2] >= min) and (datos[2] <= max):
           print(codigo, datos)

def sumarStock(diccionario):
    minstock = int(input('Agregar stock a productos que tengan stock menor a: '))
    sumar = int(input('Cuánto va a agregar:  '))
    for codigo, datos in diccionario.items():
        if (datos[2] < minstock):
            datos[2] += sumar
            print (codigo, datos)



def buscarCero(diccionario):
   cod = -1
   for codigo, datos in diccionario.items():    
        if datos[2] == 0:
           cod = codigo
   return cod 

               
#Principal
opcion = 0

os.system('cls')
while opcion != 6 :
    opcion = menu()
    if opcion == 1:
        productos = cargarProductos()
        presionarTecla()
    if opcion == 2:
        mostrar(productos)
        presionarTecla()
    if opcion == 3:
        mostrarIntervalos(productos)
        presionarTecla()
    if opcion == 4:
        sumarStock(productos)
        presionarTecla()
    if opcion == 5:
        elim = 0
        while elim != -1:
           elim = buscarCero(productos)
           if elim != -1:
             del productos[elim]
             print('Producto eliminado...')
        presionarTecla()
    if opcion == 6:
        input('Fin')